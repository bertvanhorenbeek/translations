(ns translations.core
  (:require [clojure.set :as clojureset]
            [clojure.string :as clojurestring])
  (:gen-class))

(defn open-file [filename]
  (slurp filename))


(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println (do
    (println (str "First: " (first args)))
    (println (str "Second: " (second args)))
    (let [base (map #(clojurestring/split % #"=") (filter #(not (empty? (clojurestring/triml %))) (clojurestring/split (slurp (first args)) #"\n")))
          translation (map #(clojurestring/split % #"=") (filter #(not (empty? (clojurestring/triml %))) (clojurestring/split (slurp (second args)) #"\n")))
          base-keys (into #{} (map first base))
          translation-keys (into #{} (map first translation))]
      (reduce #(str %1 "\n" %2) (clojureset/difference base-keys translation-keys))))))